import Vue from 'vue'
import Router from 'vue-router'
import DangKySuDungDien from '@/components/DangKySuDungDien/DangKySuDungDien'
import HenLichKhaoSat from '@/components/KhaoSat/HenLichKhaoSat'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/dang-ky-su-dung-dien',
      name: 'DangKySuDungDien',
      component: DangKySuDungDien
    },
    {
      path: '/hen-lich-khao-sat',
      name: 'HenLichKhaoSat',
      component: HenLichKhaoSat
    }
  ]
})
